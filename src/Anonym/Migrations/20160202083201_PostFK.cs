using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace Anonym.Migrations
{
    public partial class PostFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Post_Team_TeamID",
                table: "Post",
                column: "TeamID",
                principalTable: "Team",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Post_Team_TeamID", table: "Post");
        }
    }
}
