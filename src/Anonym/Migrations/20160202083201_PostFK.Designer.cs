using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using Anonym.Infrastructure;

namespace Anonym.Migrations
{
    [DbContext(typeof(AnonymDbContext))]
    [Migration("20160202083201_PostFK")]
    partial class PostFK
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Anonym.Model.Post", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<int>("TeamID");

                    b.Property<string>("Text")
                        .IsRequired();

                    b.HasKey("ID");
                });

            modelBuilder.Entity("Anonym.Model.Team", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<string>("Name");

                    b.Property<Guid>("TeamGuid");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("Anonym.Model.Post", b =>
                {
                    b.HasOne("Anonym.Model.Team")
                        .WithMany()
                        .HasForeignKey("TeamID");
                });
        }
    }
}
