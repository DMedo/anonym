using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace Anonym.Migrations
{
    public partial class TeamGuid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TeamGuid",
                table: "Team",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "TeamGuid", table: "Team");
        }
    }
}
