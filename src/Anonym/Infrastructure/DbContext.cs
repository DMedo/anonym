﻿using Anonym.Model;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anonym.Infrastructure
{
    public class AnonymDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }

        public DbSet<Post> Posts { get; set; }
    }
}
