﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anonym.Model
{
    public abstract class EntityBase
    {
        public int ID { get; set; }

        public DateTime Created { get; set; }

        public EntityBase()
        {
            Created = DateTime.UtcNow;
        }
    }
}
