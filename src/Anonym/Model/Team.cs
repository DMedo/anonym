﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anonym.Model
{
    public class Team : EntityBase
    {
        public Guid TeamGuid { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public Team() : base()
        {
            TeamGuid = Guid.NewGuid();
        }
    }
}
