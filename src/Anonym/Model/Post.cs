﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Anonym.Model
{
    public class Post : EntityBase
    {
        [ForeignKey("Team")]
        public virtual int TeamID { get; set; }

        [Required]
        public string Text { get; set; }

        public virtual Team Team { get; set; }
    }
}
