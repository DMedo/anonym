﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anonym.Infrastructure;
using Anonym.Model;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;

namespace Anonym.Controllers
{
    public class HomeController : Controller
    {
        private AnonymDbContext _dbContext;

        public HomeController(AnonymDbContext context)
        {
            _dbContext = context;
        }

        public IActionResult Clear()
        {
            this.DeleteSessionCookie();

            return RedirectToActionPermanent("Index");
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Index()
        {
            var team = GetTeamFromCookie();
            if (team != null)
            {
                return RedirectToAction("Posts");
            }

            return View();
        }

        [HttpPost]
        public IActionResult Index(string teamName)
        {
            var team = _dbContext.Teams
                .Where(x => x.Name == teamName)
                .FirstOrDefault();

            if (team == null)
            {
                team = new Team
                {
                    Name = teamName
                };

                _dbContext.Teams.Add(team);

                _dbContext.SaveChanges();
            }

            CreateSessionCookie(team.TeamGuid.ToString());

            return RedirectToAction("Index");
        }

        public IActionResult Posts()
        {
            var team = GetTeamFromCookie();

            if (team != null)
            {
                return View(team.Posts ?? new List<Post>());
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Posts(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return RedirectToAction("Posts");
            }

            var team = GetTeamFromCookie();

            if (team != null)
            {
                _dbContext.Posts.Add(new Post
                {
                    TeamID = team.ID,
                    Text = text
                });

                _dbContext.SaveChanges();
            }

            return RedirectToAction("Posts");
        }

        private void CreateSessionCookie(string name)
        {
            HttpContext.Response.Cookies.Append("Session", name);
        }

        private void DeleteSessionCookie()
        {
            HttpContext.Response.Cookies.Append("Session", "", new CookieOptions
            {
                Expires = DateTime.Now.AddDays(-1)
            });
        }

        private Team GetTeamFromCookie()
        {
            var cookie = ReadSessionCookie();

            return _dbContext.Teams
                .Include(x => x.Posts)
                .FirstOrDefault(x => x.TeamGuid == cookie);
        }

        private Guid ReadSessionCookie()
        {
            string cookieValue = HttpContext.Request.Cookies["Session"];
            Guid result;
            Guid.TryParse(cookieValue, out result);

            return result;
        }
    }
}